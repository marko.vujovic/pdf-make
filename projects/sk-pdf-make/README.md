# SkPdfMake

[pdfmake](https://github.com/bpampuch/pdfmake) wrapper for Angular7

## Installation

```$ npm install sk-pdf-make```

## Import

```
import {SkPdfMakeModule} from 'sk-pdf-make';
@NgModule({
  imports: [
    SkPdfMakeModule,
  ]
})
export class AppModule { }
```

`SkPdfMakeModule` has `SkPdfMakeService` added to its providers, so we can use it to generate PDF files.

## Examples

#### Creating PDF file with one empty page

```
export class AppComponent implements OnInit {

  // inject the service
  constructor(private pdfMakeService: SkPdfMakeService) { }

  ngOnInit() {
    // create document definition
    const dd: DocumentDefinition = {
      content: []
    };
    // create pdf file from specified definition and download it
    this.pdfMakeService.createPdf(dd)
      .subscribe(pdf => pdf.download());
  }

}
```

Document definition must always have `content` property.

Below is document definition with some other top-level properties and their default values:

```
{
  content: [],
  pageSize: PageSize.A4,
  pageOrientation: PageOrientation.PORTRAIT,
  compress: true,
  header: null,
  footer: null
}
```

#### Adding paragraphs to the file

```
const dd: DocumentDefinition = {
  content: [
    {
      text: 'This paragraph is bold',
      bold: true
    },
    {
      text: 'This paragraph is italics',
      italics: true
    },
    {
      text: 'This paragraph is blue',
      color: 'blue',
      fontSize: 16
    },
  ]
}
```
Paragraph can be array of texts, and each of its items can be styled individually

```
const dd: DocumentDefinition = {
  content: [
    {
      text: [
        {
          text: 'This ',
          color: 'red'
        },
        {
          text: 'paragraph ',
          color: 'blue'
        },
        {
          text: 'is ',
          color: 'indigo'
        },
        {
          text: 'colorful.',
          color: '#009f00'
        }
      ]
    }
  ]
};
```

#### Rendering tables

```
const dd: DocumentDefinition = {
  content: [
    {
      table: {
        body: [
          ['column 1', 'column 2', 'column 3'],
          ['value 1', 'value 2', 'value 3'],
          ['value 4', 'value 5', 'value 6'],
          ['value 7', 'value 8', 'value 9']
        ]
      }
    }
  ]
};
```
Customizing table header:

```
const dd: DocumentDefinition = {
  content: [
    {
      table: {
        body: [
          [
            {text: 'column 1', style: 'tableHeader'},
            {text: 'column 2', style: 'tableHeader'},
            {text: 'column 3', style: 'tableHeader'},
          ],
          ['value 1', 'value 2', 'value 3'],
          ['value 4', 'value 5', 'value 6'],
          ['value 7', 'value 8', 'value 9']
        ]
      }
    }
  ],
  styles: {
    tableHeader: {
      fillColor: '#bcbcbc',
      bold: true
    }
  }
};
```
#### Adding header and footer

```
const dd: DocumentDefinition = {
  content: [],
  header: {
    text: 'Page header goes here',
    style: 'headerStyle'
  },
  footer: (pageIndex, pageCount) => ({
    text: `${pageIndex}/${pageCount}`,
    alignment: Alignment.center
  }),
  styles: {
    headerStyle: {
      fontSize: 16,
      bold: true,
      margin: [0, 10],
      alignment: Alignment.center
    }
  }
}
```
