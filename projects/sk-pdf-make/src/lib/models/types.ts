export enum Alignment {
  left = 'left',
  right = 'right',
  center = 'center',
  justify = 'justify'
}

type Margins = number | [number, number] | [number, number, number, number];

export interface CreatedPdf {
  download: (fileName?: string) => void;
}

export interface DocumentDefinition {
  content: Array<string | Section>;
  styles?: {[key: string]: Style};
  defaultStyle?: Style;
  compress?: boolean;
  pageSize?: PageSize;
  pageOrientation?: PageOrientation;
  header?: string | Section | Section[] | HeaderFooterFunction;
  footer?: string | Section | Section[] | HeaderFooterFunction;
  info?: DocumentInformation;
  pageMargins?: Margins;
}

export interface Section {
  text?: string | Section[];
  style?: string | string[];
  table?: Table;
  columns?: Section[];
  image?: string;
  stack?: Array<string | Section>;
  bold?: boolean;
  italics?: boolean;
  color?: string;
  fillColor?: string;
  border?: boolean[] | undefined;
  fontSize?: number;
  margin?: Margins;
  alignment?: Alignment;
  ul?: Array<string | Section>;
  colSpan?: number;
  rowSpan?: number;
  layout?: string | TableLayoutFunctions;
  width?: string | number;
  height?: string | number;
}

export interface Table {
  body: Array<Array<string | Section>>;
  widths?: Array<number | '*' | 'auto' | string>;
  heights?: number[] | number;
  headerRows?: number;
}

export interface Style {
  fontSize?: number;
  bold?: boolean;
  color?: string;
  fillColor?: string;
  margin?: Array<number | 'auto'> | number | 'auto';
  alignment?: Alignment;
  // ..
}

interface TableLayoutFunctions {
  hLineWidth?: (i: number, node: any) => number;
  vLineWidth?: (i: number, node: any) => number;
  hLineColor?: (i: number, node: any) => string;
  vLineColor?: (i: number, node: any) => string;
  fillColor?: (i: number, node: any) => string;
  paddingLeft?: (i: number, node: any) => number;
  paddingRight?: (i: number, node: any) => number;
  paddingTop?: (i: number, node: any) => number;
  paddingBottom?: (i: number, node: any) => number;
}

export type HeaderFooterFunction = (currentPage: number, pageCount: number) => string | Section | Section[] | Table | Section;

export interface DocumentInformation {
  title?: string;
  author?: string;
  subject?: string;
  keywords?: string;
}

export enum PageOrientation {
  PORTRAIT = 'PORTRAIT',
  LANDSCAPE = 'LANDSCAPE'
}

export enum PageSize {
  A0_x_4 = '4A0',
  A0_x_2 = '2A0',
  AO = 'A0',
  A1 = 'A1',
  A2 = 'A2',
  A3 = 'A3',
  A4 = 'A4',
  A5 = 'A5',
  A6 = 'A6',
  A7 = 'A7',
  A8 = 'A8',
  A9 = 'A9',
  A1O = 'A10',
  BO = 'B0',
  B1 = 'B1',
  B2 = 'B2',
  B3 = 'B3',
  B4 = 'B4',
  B5 = 'B5',
  B6 = 'B6',
  B7 = 'B7',
  B8 = 'B8',
  B9 = 'B9',
  B1O = 'B10',
  CO = 'C0',
  C1 = 'C1',
  C2 = 'C2',
  C3 = 'C3',
  C4 = 'C4',
  C5 = 'C5',
  C6 = 'C6',
  C7 = 'C7',
  C8 = 'C8',
  C9 = 'C9',
  C1O = 'C10',
  RA1 = 'RA1',
  RA2 = 'RA2',
  RA3 = 'RA3',
  RA4 = 'RA4',
  SRA1 = 'SRA1',
  SRA2 = 'SRA2',
  SRA3 = 'SRA3',
  SRA4 = 'SRA4',
  EXECUTIVE = 'EXECUTIVE',
  FOLIO = 'FOLIO',
  LEGAL = 'LEGAL',
  LETTER = 'LETTER',
  TABLOID = 'TABLOID'
}
