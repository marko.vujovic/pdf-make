export class Script {
  constructor(
    public name: string,
    public src: string,
    public loaded = false
  ) { }
}
