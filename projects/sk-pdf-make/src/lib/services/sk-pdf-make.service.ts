import {SkScriptLoaderService} from './sk-script-loader.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Script} from '../models/script';
import {map, skip} from 'rxjs/operators';
import {CreatedPdf, DocumentDefinition} from '../models/types';
import {Inject, Injectable, Optional} from '@angular/core';
import {SK_PDF_MAKE_CONFIG, SkPdfMakeConfig} from '../sk-pdf-make.module';

@Injectable()
export class SkPdfMakeService {

  private _pdfMakeScript = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.54/pdfmake.min.js';
  private _pdfFontsScript = 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.54/vfs_fonts.js';
  private _pdfMake: {vfs: any, createPdf: (dd: DocumentDefinition) => CreatedPdf};
  private _pdfMakeLoaded = new BehaviorSubject<boolean>(false);
  private _loadingScripts = false;

  constructor(
    @Inject(SK_PDF_MAKE_CONFIG) @Optional() private _config: SkPdfMakeConfig,
    private _scriptLoaderService: SkScriptLoaderService,
  ) {
    if (_config) {
      if (_config.fontsLink) {
        this._pdfFontsScript = _config.fontsLink;
      }
      if (_config.scriptLink) {
        this._pdfMakeScript = _config.scriptLink;
      }
      if (_config.loadOnInit) {
        this.loadScripts();
      }
    }
  }

  createPdf(documentDefinition: DocumentDefinition): Observable<CreatedPdf> {
    if (this._pdfMake) {
      return of(this._pdfMake.createPdf(documentDefinition));
    }
    if (!this._loadingScripts) {
      this.loadScripts();
    }
    return this._pdfMakeLoaded.pipe(
      skip(1),
      map(() => this._pdfMake.createPdf(documentDefinition))
    );
  }

  loadScripts() {
    if (this._pdfMakeLoaded.value) {
      throw new Error('pdf make scripts already loaded');
    }
    this._loadingScripts = true;
    this._scriptLoaderService.load(new Script('pdfMake', this._pdfMakeScript))
      .subscribe(() => {
        this._scriptLoaderService.load(new Script('pdfFonts', this._pdfFontsScript))
          .subscribe(() => {
            this._pdfMake = window['pdfMake'];
            this._pdfMakeLoaded.next(true);
            this._pdfMakeLoaded.complete();
            this._loadingScripts = false;
          });
      });
  }

}
