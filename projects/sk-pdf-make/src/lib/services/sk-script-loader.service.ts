import {Observable, Observer} from 'rxjs';
import {Script} from '../models/script';
import {Injectable} from '@angular/core';

// https://stackoverflow.com/a/44016611
@Injectable()
export class SkScriptLoaderService {

  private _loadedScripts: Script[] = [];

  /**
   * Dynamically loads JS script
   * @param script - script to be loaded
   */
  public load(script: Script): Observable<Script> {
    return new Observable<Script>((observer: Observer<Script>) => {
      const existingScript = this._loadedScripts.find(s => s.name === script.name);
      // Complete if already loaded
      if (existingScript && existingScript.loaded) {
        observer.next(existingScript);
        observer.complete();
      } else {
        // Add the script
        this._loadedScripts.push(script);
        // create script element
        const scriptElement = document.createElement('script') as any;
        scriptElement.type = 'text/javascript';
        scriptElement.src = script.src;
        // file is loaded and script is executed
        scriptElement.onload = () => {
          script.loaded = true;
          observer.next(script);
          observer.complete();
        };
        // file is loaded and script is executed (this is required for IE)
        if (scriptElement.readyState) {
          scriptElement.onreadystatechange = () => {
            if (scriptElement.readyState === 'loaded' || scriptElement.readyState === 'complete') {
              scriptElement.onreadystatechange = null;
              scriptElement.onload();
            }
          };
        }
        // couldn't load script
        scriptElement.onerror = () => {
          observer.error(`Couldn't load script ${script.src}`);
        };
        // make the request for the file
        document.getElementsByTagName('body')[0].appendChild(scriptElement);
      }
    });
  }

}
