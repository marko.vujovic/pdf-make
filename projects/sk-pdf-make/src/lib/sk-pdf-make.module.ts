import {InjectionToken, ModuleWithProviders, NgModule} from '@angular/core';
import {SkScriptLoaderService} from './services/sk-script-loader.service';
import {SkPdfMakeService} from './services/sk-pdf-make.service';

export interface SkPdfMakeConfig {
  scriptLink?: string;
  fontsLink?: string;
  loadOnInit?: boolean;
}

export const SK_PDF_MAKE_CONFIG = new InjectionToken<SkPdfMakeConfig>('SkPdfMakeConfig');

@NgModule({})
export class SkPdfMakeModule {

  static forRoot(config: SkPdfMakeConfig): ModuleWithProviders {
    return {
      ngModule: SkPdfMakeModule,
      providers: [
        SkScriptLoaderService,
        SkPdfMakeService,
        {provide: SK_PDF_MAKE_CONFIG, useValue: config},
       ]
    };
  }

}

